from subprocess import check_output, Popen
import sys

flags = {}
bin_names = {1:"blackbox_easy",2:"blackbox_hard"}
solved = []
NUM_STAGES = len(bin_names)
threshold = 100
difficulties = {"baby":10,"easy":20,"hard":30,"baby?":50}
descriptions = {1:"easy",2:"hard"}

def load_flags():
	global flags
	for i in range(1,NUM_STAGES+1):
		try:
			fp = open("flag%d"%i,"r")
			flags[i] = fp.read()
			fp.close()
		except:
			print "[!] fatal error. contact admin."
			exit(0)

def print_main_menu():
	print "Choose your stage from 1~%d"%NUM_STAGES

def print_sub_menu(idx):
	print "Difficulty: %s"%descriptions[idx]
	print "[1] Get binary"
	print "[2] X-ploit time!"
	print "[3] Authenticate flag"
	print "[4] I don't wanna look at this anymore..."

def get_option(maxval):
	try:
		num = int(raw_input())
		if num > maxval:
			return -1
		return num
	except:
		return -1

def intro():
	print "There are %d binaries in here that you must pwn."%NUM_STAGES
	print "For each difficulty the points you can get are different"
	print "="*40
	for x in difficulties:
		print "%s: %dpts"%(x,difficulties[x])
	print "="*40
	print "If you reach %d points I will give you the flag!"%threshold

def check_points():
	score = 0
	for x in solved:
		score+=difficulties[descriptions[x]]

	if score>=100:
		print "Here is your flag: flag{XXXXXXX}"

def main():
	intro()
	while True:
		check_points()
		print_main_menu()
		choice = get_option(NUM_STAGES)
		if choice in solved:
			print "[*] You already solved this challenge"
			continue
		handler(choice)



def handler(idx):
	global solved
	bin_name = bin_names[idx]

	while True:
		print_sub_menu(idx)
		option = get_option(4)
		if option == 2:
			proc = Popen(["./%s"%bin_name],stdin=sys.stdin,stdout=open("/dev/null"),stderr=sys.stderr)
			proc.communicate()
		elif option == 1:
			fp = open(bin_name,"r")
			data = fp.read()
			encoded = data.encode('base64').replace("\n","")
			print "[+] Here is your binary: %s"%encoded
		elif option == 3:
			inpt = raw_input()
			if inpt == flags[idx]:
				solved.append(idx)
				print "[+] Congratz! You solved this challenge."
			else:
				print "[-] Your flag is wrong... sorry"
		else:
			return

if __name__ == "__main__":
	load_flags()
	main()