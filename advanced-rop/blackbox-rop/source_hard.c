#include <stdio.h>
#include <stdlib.h>
#include <sys/prctl.h> 
#include <seccomp.h>
#include <unistd.h>

void read_file(){
	char * fname = malloc(0x80);
	
	read(0,fname,0x80);
	
	if(strncmp(fname,"flag",5)){
		exit(0);
	}
	int fd = open(fname,0);
}

void enable_sandbox(){
	prctl(PR_SET_NO_NEW_PRIVS, 1);
  // ensure no escape is possible via ptrace
	prctl(PR_SET_DUMPABLE, 0);
  
  // Init the filter
	scmp_filter_ctx ctx;
	ctx = seccomp_init(SCMP_ACT_KILL); // default action: kill
  // setup basic whitelist
	seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(open), 0);
	seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0);
	seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0);
	seccomp_load(ctx);
  }

int main(int argc, char ** argv){
	enable_sandbox();
	char buf[0x10];
	read(0,buf,0x200);
	close(0);
	close(1);
	close(2);
	return 0;
}
