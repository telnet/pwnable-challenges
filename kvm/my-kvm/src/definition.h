#define MEMORY_LIMIT 0x4000000
#define STACK_LIMIT 0x1000000
#define STACK_BASE 0x200000

typedef struct VM_s{
	int kvmfd;
	int vmfd;
	int vcpufd;
	size_t vcpu_size;
	void * mem;
} VM;