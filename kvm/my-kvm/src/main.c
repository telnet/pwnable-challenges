#include <fcntl.h>
#include <linux/kvm.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include "definition.h"
#include "utils.h"

VM vm;
struct kvm_run * run;

void init_vm(char * code){
	vm.kvmfd = open("/dev/kvm",O_RDWR|O_CLOEXEC);
	vm.vmfd = ioctl(vm.kvmfd,KVM_CREATE_VM,0);
	struct kvm_userspace_memory_region region;


	//set vm memory
	size_t mem_size = MEMORY_LIMIT;
	void * mem = _Mmap(0,mem_size,PROT_READ|PROT_WRITE,MAP_SHARED|MAP_ANONYMOUS,-1,0);
	int code_offset = 0;

	region.slot = 0;
	region.flags = 0;
	region.guest_phys_addr = 0;
	region.memory_size = mem_size;
	region.userspace_addr = (size_t) mem;

	ioctl(vm.vmfd,KVM_SET_USER_MEMORY_REGION,&region);

	//setup vcpu
	vm.vcpufd = ioctl(vm.vmfd,KVM_CREATE_VCPU,0);
	size_t vcpu_mmap_size = ioctl(vm.kvmfd,KVM_GET_VCPU_MMAP_SIZE,0);
	run = (struct kvm_run *)mmap(0,vcpu_mmap_size,PROT_READ|PROT_WRITE,MAP_SHARED,vm.vcpufd,0);

	//setup registers
	struct kvm_regs regs;
	ioctl(vm.vcpufd,KVM_GET_REGS,&regs);
	regs.rip = code_offset;
	regs.rsp = STACK_BASE;
	regs.rflags = 0x2;
	ioctl(vm.vcpufd,KVM_SET_REGS,&regs);

	struct kvm_sregs sregs;
	ioctl(vm.vcpufd,KVM_GET_SREGS,&sregs);
	sregs.cs.base = sregs.cs.selector = 0;
	ioctl(vm.vcpufd,KVM_SET_REGS,&sregs);
}

void execute(){
	while(1){
		ioctl(vm.vcpufd,KVM_RUN,0);
		switch(run->exit_reason){
			case KVM_EXIT_HLT:
				puts("KVM_EXIT_HLT");
				return;
			case KVM_EXIT_IO:
				putchar(*(((char *)run) + run->io.data_offset));
      			break;
      		default:
      			puts("[!] unknown abort.");
      			return;
		}
	}
}

int main(int argc, char ** argv, char *** envp){
	if(argc!=2){
		puts("[-] usage: ./hypervisor binary");
	}
	char * code = read_file(argv[1]);
	init_vm(code);
	execute();

	return 0;
}