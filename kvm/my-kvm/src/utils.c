#include <fcntl.h>
#include <linux/kvm.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

int _Open(char *fname, int mode){
	int fd = open(fname,mode);
	if(fd == -1){
		puts("[-] open() error.");
		exit(-1);
	}
	return fd;
}

void * _Mmap(void * addr, size_t length, int prot, int flags, int fd, size_t offset){
	void * retaddr = mmap(addr,length,prot,flags,fd,offset);
	if(retaddr == -1 || addr !=NULL && retaddr!=addr){
		puts("[-] mmap() error.");
		exit(-1);
	}
	return retaddr;
}

int _Read(int fd, char * buf, size_t size){
	size_t numbytes = read(fd,buf,size);
	if(numbytes == -1){
		puts("[-] read() error.");
		exit(-1);
	}
	return numbytes;
}

void * _Malloc(size_t size){
	void * addr = malloc(size);
	if(addr == NULL){
		puts("[-] malloc() error.");
		exit(-1);
	}
	return addr;

}
char * read_file(char * fname){
	struct stat st;
	int retval = stat(fname,&st);
	if(retval == -1){
		puts("[-] stat() error.");
		exit(-1);
	}
	size_t filesize = st.st_size;
	char * buf = _Malloc(filesize);

	int fd = open(fname,0);
	_Read(fd,buf,filesize);

	return buf;

}