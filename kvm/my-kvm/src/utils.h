int _Open(char * fname, int mode);
void * _Mmap(void * addr, size_t length, int prot, int flags, int fd, size_t offset);
int _Read(int fd, char * buf, size_t size);
void * _Malloc(size_t size);
char * read_file(char * fname);
